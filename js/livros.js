fetch("../livros.json")
    .then(function(resp) {
        return resp.json();
    })
    .then(function(data) {
        let livros = data.livros;
        const tabela = document.getElementById("tabela");
        livros.forEach(function(livro, index) {
            // Nome do Livro
            let nome = document.createElement("span");
            nome.classList.add("tabela__item");
            nome.classList.add("tabela__item--nome");
            nome.innerText = livro.nome;
            tabela.appendChild(nome);

            // Autor do Livro
            let autor = document.createElement("span");
            autor.classList.add("tabela__item");
            autor.classList.add("tabela__item--autor");
            autor.innerText = livro.autor;
            tabela.appendChild(autor);

            // Preço do Livro
            let preco = document.createElement("span");
            preco.classList.add("tabela__item");
            preco.classList.add("tabela__item--preco");
            if(typeof livro.preco == "number"){
                preco.innerText = 'R$ ' + livro.preco.toFixed(2).toString().replace(".", ",");
            }else {
                preco.innerText = livro.preco;
            }
            tabela.appendChild(preco);

            // Botão de Opções e Dropdown
            let opcoes = document.createElement("div");
            let opcoes__button = document.createElement("button");
            let opcoes__dropdown= document.createElement("div");
            let editar = document.createElement("a");
            let excluir = document.createElement("a");

            opcoes.classList.add("tabela__item");
            opcoes.classList.add("opcoes");
            opcoes__button.classList.add("opcoes__button");
            opcoes__dropdown.classList.add("opcoes__dropdown");
            opcoes__dropdown.setAttribute("id", "dropdown")

            opcoes__button.innerText = "Opções";
            editar.innerText = "Editar";
            editar.href = "./cadastro.html"
            excluir.innerText = "Excluir";
            excluir.href = "#"

            tabela.appendChild(opcoes);
            opcoes.appendChild(opcoes__button);
            opcoes.appendChild(opcoes__dropdown);
            opcoes__dropdown.appendChild(editar);
            opcoes__dropdown.appendChild(excluir);

            opcoes__button.onclick = () => {
                document.getElementsByClassName("opcoes__dropdown")[index].classList.toggle("show");
            }

            // Fecha o dropdown ao clicar fora da div
            window.onclick = function(event) {
                let x = document.getElementsByClassName("opcoes__button")[index];
                if (!event.target.matches('.opcoes__button')) {
                  let dropdowns = document.getElementsByClassName("opcoes__dropdown");
                  for (let i = 0; i < dropdowns.length; i++) {
                    let openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                      openDropdown.classList.remove('show');
                    }
                  }
                }
            }

            // Editar livro
            editar.onclick = () => {
                let id = livro.id;
                let nome = livro.nome;
                let autor = livro.autor;
                let preco = livro.preco;

                sessionStorage.setItem("ID", id);
                sessionStorage.setItem("NOME", nome);
                sessionStorage.setItem("AUTOR", autor);
                sessionStorage.setItem("PREÇO", preco);

                return
            }

            // Excluir livro
            excluir.onclick = () => {
                fetch("http://localhost:3000/livros/"+livro.id,{
                method:'DELETE',
                body:JSON.stringify({
                    nome:nome,
                    autor:autor,
                    preco:parseFloat(preco)
                }),
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                }
                })
                .then(function(response){
                    return response.json()
                })
                .then(function(data){
                    console.log(data)
                })
            }
        }); 
    });
