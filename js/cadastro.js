let cadastro_form = document.getElementById("cadastro-form");

window.addEventListener('load', () => {
    document.getElementById("cadastro-nome").value = "";
    document.getElementById("cadastro-autor").value = "";
    document.getElementById("cadastro-preco").value = "";
})

cadastro_form.addEventListener('submit', function(e){
    e.preventDefault()

    let nome = document.getElementById("cadastro-nome").value;
    let autor = document.getElementById("cadastro-autor").value;
    let preco = document.getElementById("cadastro-preco").value;

    fetch("http://localhost:3000/livros",{
        method:'POST',
        body:JSON.stringify({
            nome:nome,
            autor:autor,
            preco:parseFloat(preco)
        }),
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        }
    })
    .then(function(response){
        return response.json()
    })
    .then(function(data){
        console.log(data)
    })
})

function soNumero(event) {
    let char = String.fromCharCode(event.which);
    if(!(/[0-9]/.test(char)) && !(/[.]/.test(char))){
        event.preventDefault();
    }
}