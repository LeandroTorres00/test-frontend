let editar_form = document.getElementById("editar-form");

fetch("../livros.json")
    .then(function(resp) {
        return resp.json();
    })
    .then(function(data) { 
        document.getElementById("editar-id").max = data.livros.length;
    })

window.addEventListener('load', () => {
    let id = sessionStorage.getItem("ID");
    let nome = sessionStorage.getItem("NOME");
    let autor = sessionStorage.getItem("AUTOR");
    let preco = sessionStorage.getItem("PREÇO");

    document.getElementById("editar-id").value = id;
    document.getElementById("editar-nome").value = nome;
    document.getElementById("editar-autor").value = autor;
    document.getElementById("editar-preco").value = preco;
})

window.onunload = function () {
	sessionStorage.removeItem('ID');
	sessionStorage.removeItem('NOME');
	sessionStorage.removeItem('AUTOR');
	sessionStorage.removeItem('PREÇO');
}

editar_form.addEventListener('submit', function(e){
    e.preventDefault()
    
    let id = document.getElementById("editar-id").value;

    let nome = document.getElementById("editar-nome").value;
    let autor = document.getElementById("editar-autor").value;
    let preco = document.getElementById("editar-preco").value;

    

    fetch("http://localhost:3000/livros/"+id,{
        method:'PUT',
        body:JSON.stringify({
            nome:nome,
            autor:autor,
            preco:parseFloat(preco)
        }),
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        }
        })
        .then(function(response){
            return response.json()
        })
        .then(function(data){
            console.log(data)
        })
})